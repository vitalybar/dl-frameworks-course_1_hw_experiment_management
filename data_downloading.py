import os
import wget
from pandas import read_csv


def data_downloading(data_url: str, features_csv: str, labels_csv: str):
    """
    The method preparing and processing features and labels
    :param data_url: URL to data file
    :param features_csv: path to features
    :param labels_csv: path to labels
    :return: create features.csv and labels.csv
    """
    if not os.path.isfile("data/raw/raw_data.csv"):
        wget.download(data_url, "data/raw/raw_data.csv")

    data = read_csv("data/raw/raw_data.csv", delimiter=";", header=0)
    # get features and labels
    features = data["comment"]
    # rounding of estimates to simplify training
    labels = data["isHate"]

    features.to_csv(features_csv)
    labels.to_csv(labels_csv)


if __name__ == "__main__":
    data_downloading(
        "https://raw.githubusercontent.com/intelligence-csd-auth-gr/"
        "Ethos-Hate-Speech-Dataset/master/ethos/ethos_data/"
        "Ethos_Dataset_Binary.csv",
        "data/raw/features.csv",
        "data/raw/labels.csv",
    )

import os

import optuna
import json
import pickle
import pandas as pd
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from dvclive import Live

# List of experiments
experiments = []


def vectorization(
    processed_features: list,
    labels: list,
    max_features: int,
    max_df: float,
    test_size: float,
):
    """
    Text vectorization func
    :param labels: labels of terms list
    :param processed_features: terms list
    :param max_features: features max threshold
    :param max_df: max frequency terms threshold
    :param test_size: proportion of test and train data
    :return: lists of test and train data
    """
    vectorizer = TfidfVectorizer(
        max_features=max_features,
        min_df=0.0001,
        max_df=max_df,
        stop_words=stopwords.words("english"),
    )
    features: object = vectorizer.fit_transform(processed_features).toarray()

    # split data to train
    X_train, X_test, y_train, y_test = train_test_split(
        features, labels, test_size=test_size, random_state=0
    )
    return X_train, X_test, y_train, y_test


def model_train(n_estimators: int, X_train: list, y_train: list):
    """
    Random forest train function
    :param n_estimators: nuber of forest trees
    :param X_train: train features
    :param y_train: train labels
    :return: model of RF_classifier
    """
    text_classifier = RandomForestClassifier(
        n_estimators=n_estimators, random_state=4221
    )

    with open("models/random_forest_model.pickle", "wb") as mod:
        mod.write(pickle.dumps(text_classifier))

    with Live(save_dvc_exp=True) as live:
        live.log_artifact("models/random_forest_model.pickle")
        live.log_metric("n_estimators", n_estimators)

    # train
    text_classifier.fit(X_train, y_train)

    return text_classifier


def save_model(path: str, params: dict):
    """
    Save model function
    :param path: path to saving model
    :param params: params dict
    :return: None
    """

    X_train, _, y_train, _ = vectorization(
        processed_features,
        labels,
        params["max_features"],
        params["max_df"],
        params["test_size"],
    )

    # train model with best params
    rf_model = model_train(params["n_estimators"], X_train, y_train)

    # save rf_model
    with open(path, mode="wb") as random_forest:
        pickle.dump(rf_model, random_forest)


def objective(trial: optuna.Trial):
    """
    Optuna trial function
    :param trial: optuna.Trial object
    :return: accuracy_score, parameters dictionary of trial
    """
    params = {
        "number": trial.number,
        "max_features": trial.suggest_int("max_features", 100, 1000),
        "max_df": trial.suggest_float("max_df", 0.5, 1.0),
        "n_estimators": trial.suggest_int("n_estimators", 10, 1000),
        "test_size": trial.suggest_float("test_size", 0.2, 0.5),
        "score": 0,
    }

    X_train, X_test, y_train, y_test = vectorization(
        processed_features,
        labels,
        params["max_features"],
        params["max_df"],
        params["test_size"],
    )

    # train and eval metrics
    predictions = model_train(params["n_estimators"], X_train, y_train).predict(X_test)

    params["score"] = accuracy_score(y_test, predictions)
    experiments.append(params)
    return accuracy_score(y_test, predictions), params["n_estimators"]


if __name__ == "__main__":
    # Storage to save study data
    storage = "postgresql://root:root@localhost:5432/optuna_db"
    path_to_trials_data = "trials_log.csv"

    processed_features = pd.read_csv("data/processed/features.csv")["0"].tolist()
    labels = pd.read_csv("data/processed/labels.csv")["0"].tolist()

    # Start optuna study with maximizing accuracy and minimizing n_estimators
    study = optuna.create_study(
        study_name="random_forest_study",
        directions=["maximize", "minimize"],
        storage=storage,
        load_if_exists=True,
    )
    study.set_metric_names(["Accuracy", "n_estimators"])
    study.optimize(objective, n_trials=5)

    df = study.trials_dataframe(attrs=("number", "value", "params", "state"))

    print(df)
    # fig = optuna.visualization.plot_param_importances(study,
    #                                             target=lambda t: t.values[0])
    # fig.show()
    # fig = optuna.visualization.plot_param_importances(study,
    #                                             target=lambda t: t.values[1])
    # fig.show()

    # parameters_importance = optuna.importance.get_param_importances(study,
    #                                             target=lambda t: t.values[1])
    # print(parameters_importance)

    # Save trials data
    df.to_csv(path_to_trials_data)

    # Save best trial params
    json_params = json.dumps(study.best_trials[0].params)
    with open("data.txt", "w") as outfile:
        json.dump(json_params, outfile)

    # Save model with best params with pickle
    save_model("models/random_forest_model.pickle", study.best_trials[0].params)

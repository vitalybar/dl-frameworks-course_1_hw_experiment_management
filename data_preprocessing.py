"""Module preprocessing data"""

import re
import pandas as pd
from pandas import read_csv


def feature_processing(features_csv: str, labels_csv: str):
    """
    The method preparing and processing features and labels
    :param features_csv: path to prepared features
    :param labels_csv: path to labels
    :return: preprocessed features.csv
    """

    # get features and labels
    features = read_csv(features_csv)["comment"].values
    processed_labels = read_csv(labels_csv)["isHate"].astype("float").round(0).values

    # preprocessing data
    processed_features: list[str] = []
    for sentence in enumerate(features):
        # Remove all the special characters
        processed_feature = re.sub(r"\W", " ", str(sentence))

        # remove all single characters
        processed_feature = re.sub(r"\s+[a-zA-Z]\s+", " ", processed_feature)

        # Remove single characters from the start
        processed_feature = re.sub(r"\^[a-zA-Z]\s+", " ", processed_feature)

        # Substituting multiple spaces with single space
        processed_feature = re.sub(r"\s+", " ", processed_feature, flags=re.I)

        # Removing prefixed 'b'
        processed_feature = re.sub(r"^b\s+", "", processed_feature)

        # Converting to Lowercase
        processed_feature = processed_feature.lower()

        processed_features.append(processed_feature)

    pd.DataFrame(processed_features).to_csv("data/processed/features.csv")
    pd.DataFrame(processed_labels).to_csv("data/processed/labels.csv")
    print("Features and labels preprocessed successfully")


if __name__ == "__main__":
    feature_processing("data/raw/features.csv", "data/raw/labels.csv")

## Разработка приложений разговорного ИИ
### Домашнее задание "Experiment management"

##### Dataset
Для обработки был взят датасет для бинарной классификации хейт-спича(eng) (https://github.com/intelligence-csd-auth-gr/Ethos-Hate-Speech-Dataset/blob/master/ethos/ethos_data/Ethos_Dataset_Binary.csv).
Значения для бинарной классификации были просто округлены. Так же в качестве предобработки были удалены стоп слова и проведена TFIDF векторизация.
##### Model Selection
В качестве обучаемой модели был выбран random forest.
В качестве оптимизируемых гиперпараметров были выбраны:
* `max_features` - параметр векторизатора, определяющий, какие слова попадут конечную выборку
* `max_df` - параметр векторизатора, определяющий долю документов для
* `n_estimators` - количество деревьев в random forest
* `test_size` - разбиение датасета на тренировочную и тестовую часть

```def objective(trial: optuna.Trial):
    """
    Optuna trial function
    :param trial: optuna.Trial object
    :return: accuracy_score, parameters dictionary of trial
    """
    params = {
        "number": trial.number,
        "max_features": trial.suggest_int("max_features", 100, 1000),
        "max_df": trial.suggest_float("max_df", 0.5, 1.0),
        "n_estimators": trial.suggest_int("n_estimators", 10, 1000),
        "test_size": trial.suggest_float("test_size", 0.2, 0.5),
        "score": 0,
    }

    X_train, X_test, y_train, y_test = vectorization(
        processed_features,
        labels,
        params["max_features"],
        params["max_df"],
        params["test_size"],
    )

    # train and eval metrics
    predictions = model_train(params["n_estimators"], X_train, y_train).predict(X_test)

    params["score"] = accuracy_score(y_test, predictions)
    experiments.append(params)
    return accuracy_score(y_test, predictions), params["n_estimators"]
```

##### Hyperparameter Optimization
В качестве инструмента подбора поиска наилучших параметров была выбрана Optuma.
В работе был настроен storage на основе поднятой локально posrtges, все получаемые гиперпараметры так же были сохранены в файле `trials_log.csv`. В ходе экспериментов оптимизировались два параметра: accuracy и n_estimators, соответственно в направлениях максимизации и минимизации.

```   # Start optuna study with maximizing accuracy and minimizing n_estimators
    study = optuna.create_study(
        study_name="random_forest_study",
        directions=["maximize", "minimize"],
        storage=storage,
        load_if_exists=True,
    )
```
##### Experiment Tracking
Для трекинга экспериментов, метрик и модели использовался DVC. Был настроен локальный репозиторий DVC, настроено отслеживание данных, метрик при тренировке модели c DVCLive (почему то он не логгирует все данные, а только последний проведенный эксперимент), сохраняемых моделей. Графики красивые прикрутить не получилось, так как не получилось нормально логгировать метрики.

```
    with Live(save_dvc_exp=True) as live:
        live.log_artifact("models/random_forest_model.pickle")
        live.log_metric("n_estimators", n_estimators)
```
##### Summary report
Автоматизация подбора гиперпараметров, а если это еще и приправляется хорошими графиками, снятых во время проводимых испытаний - очень полезная штука, упрощающая жизнь ML-разработчика. Версионирование данных и получаемых моделей - собственно так же как и git в разработке - штука неотъемлемая и полезная. Данные инструменты явно требуют более глубокого изучения, но даже поверхностно потрогать, как они работают на реальной практике - безумно полезный опыт.
prepare_stage1:
	python data_downloading.py

prepare_stage2:
	python data_preprocessing.py

data_prepare:
	prepare_stage1 prepare_stage2

training:
	python model_train.py